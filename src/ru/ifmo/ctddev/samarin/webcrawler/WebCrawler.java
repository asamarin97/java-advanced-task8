package ru.ifmo.ctddev.samarin.webcrawler;

import info.kgeorgiy.java.advanced.crawler.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This class provides methods for recursive scan of website pages.
 */
public class WebCrawler implements Crawler {
    private final ResourceExecutor downloaders;
    private final ExecutorService extractors;
    private final Downloader downloader;

    /**
     * Creates an instance of {@code WebCrawler}
     * @param downloader downloader used to download pages
     * @param numDownloaders maximum numbers of threads used to download pages
     * @param numExtractors maximum number of threads used to extract links
     * @param maxConnectionsPerHost maximum connection number to the same host
     */
    public WebCrawler(Downloader downloader, int numDownloaders, int numExtractors, int maxConnectionsPerHost) {
        this.downloader = downloader;
        this.downloaders = new ResourceExecutor(Executors.newFixedThreadPool(numDownloaders), maxConnectionsPerHost);
        this.extractors = Executors.newFixedThreadPool(numExtractors);
    }

    /**
     * Download all pages recursively accessible by links from {@code url}.
     * @param url url to start from
     * @param depth depth of recursion (1 = check only {@code url}, 2 = follow one level of links from {@code url})
     * @return result object containing all accessible links and errors
     */
    public Result download(String url, int depth) {
        DownloadTreeRequest req = new DownloadTreeRequest(url, depth);
        try {
            req.execute();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return new Result(req.getVisitedUrls(), req.getErrors());
    }

    /**
     * Closes resources used by this instance.
     * Subsequent calls to {@link #download(String, int)} lead to undefined behavior.
     */
    public void close() {
        downloaders.shutdownNow();
        extractors.shutdownNow();
    }

    private class DownloadTreeRequest {
        private final Set<String> foundLinks;
        private final Set<String> downloadedLinks;
        private final Map<String, IOException> errors;
        private final AtomicLong tasksInProgress;

        private final String startUrl;
        private final int depth;

        private DownloadTreeRequest(String url, int depth) {
            this.foundLinks = new HashSet<>();
            this.downloadedLinks = new HashSet<>();
            this.errors = new HashMap<>();
            this.tasksInProgress = new AtomicLong();
            this.depth = depth;
            this.startUrl = url;
        }

        private void execute() throws InterruptedException {
            addTask(startUrl, depth);
            synchronized (tasksInProgress) {
                while (tasksInProgress.get() != 0) {
                    tasksInProgress.wait();
                }
            }
        }

        private List<String> getVisitedUrls() {
            synchronized (downloadedLinks) {
                return new ArrayList<>(downloadedLinks);
            }
        }

        private Map<String, IOException> getErrors() {
            synchronized (errors) {
                return errors;
            }
        }

        private void extractLinks(DownloadTask task) {
            try {
                List<String> links = task.getDocument().extractLinks();

                int newDepthLeft = task.depthLeft - 1;
                links.stream().distinct().forEach((link) -> addTask(link, newDepthLeft));
                task.completeExtraction();
                task.finish();
            } catch (IOException e) {
                task.error(e);
            }
        }

        private void addTask(String link, int depth) {
            synchronized (foundLinks) {
                if (!foundLinks.add(link)) {
                    return;
                }
            }
            DownloadTask task = new DownloadTask(link, depth);

            try {
                String host = URLUtils.getHost(link);

                downloaders.submit(host, () -> {
                    String url = task.getUrl();
                    try {
                        Document document = downloader.download(url);
                        task.completeDownload(document);

                        synchronized (downloadedLinks) {
                            downloadedLinks.add(task.getUrl());
                        }

                        if (task.depthLeft > 1) {
                            extractors.submit(() -> extractLinks(task));
                        } else {
                            task.finish();
                        }
                    } catch (IOException e) {
                        task.error(e);
                    }
                });
            } catch (MalformedURLException e) {
                task.error(e);
            }
        }

        private final class DownloadTask {
            private final String url;
            private final int depthLeft;
            private volatile TaskState state;
            private volatile Document document;

            public DownloadTask(String url, int depthLeft) {
                this.url = url;
                this.state = TaskState.WAITING;
                this.depthLeft = depthLeft;
                tasksInProgress.incrementAndGet();
            }

            private String getUrl() {
                return url;
            }

            private void completeDownload(Document document) {
                assert (state == TaskState.WAITING);
                this.state = TaskState.DOWNLOADED;
                this.document = document;
            }

            private void completeExtraction() {
                assert (state == TaskState.DOWNLOADED);
                this.state = TaskState.EXTRACTED;
            }

            private void finish() {
                assert (state != TaskState.FINISHED && state != TaskState.ERROR);
                state = TaskState.FINISHED;

                taskCompleted();
            }

            private void error(IOException exception) {
                assert (state != TaskState.EXTRACTED);

                this.state = TaskState.ERROR;

                synchronized (errors) {
                    //noinspection ThrowableResultOfMethodCallIgnored
                    errors.put(url, exception);
                }

                taskCompleted();
            }

            private void taskCompleted() {
                long tasksLeft = tasksInProgress.decrementAndGet();
                if (tasksLeft == 0) {
                    synchronized (tasksInProgress) {
                        tasksInProgress.notify();
                    }
                }
            }

            private Document getDocument() {
                return document;
            }
        }
    }


    private enum TaskState {
        WAITING, DOWNLOADED, EXTRACTED, FINISHED, ERROR
    }
}
