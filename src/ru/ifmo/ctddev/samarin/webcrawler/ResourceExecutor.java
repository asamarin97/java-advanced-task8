package ru.ifmo.ctddev.samarin.webcrawler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;

/**
 * Executes tasks concurrently with respect to limited resources.
 * Each task has some bound resource and number of concurrently running tasks with the same resource is limited.
 */
public class ResourceExecutor {
    private final Map<Object, ResourceData> resourcesMap = new HashMap<>();
    private final int maxThreadsForResource;
    private final ExecutorService executor;

    /**
     * Creates an instance of {@code ResourceExecutor}
     *
     * @param executor              executor used to run tasks
     * @param maxThreadsForResource amount of tasks using the same resource which can be executed concurrently
     */
    public ResourceExecutor(ExecutorService executor, int maxThreadsForResource) {
        this.executor = executor;
        this.maxThreadsForResource = maxThreadsForResource;
    }

    /**
     * Submits a task to executor.
     *
     * @param resource resource bound to the task
     * @param runnable task to execute
     */
    public void submit(Object resource, Runnable runnable) {
        getResourceData(resource).submit(runnable);
    }

    /**
     * Shutdowns the instance of executor. Subsequent calls to {@link #submit} lead to undefined behavior.
     * Backing executor is shut down too.
     * This method does not wait for currently executing tasks to finish execution.
     */
    public void shutdownNow() {
        executor.shutdownNow();
    }

    private ResourceData getResourceData(Object resource) {
        synchronized (resourcesMap) {
            ResourceData resourceData = resourcesMap.get(resource);
            if (resourceData == null) {
                resourceData = new ResourceData();
                resourcesMap.put(resource, resourceData);
            }
            return resourceData;
        }
    }

    private class ResourceData {
        private final Queue<Runnable> tasksQueue = new LinkedList<>();
        private int tasksExecuting;

        private synchronized void submit(Runnable runnable) {
            tasksQueue.add(runnable);
            addNextTaskToExecutorIfPossible();
        }

        private synchronized void handleTaskCompletion() {
            tasksExecuting--;
            addNextTaskToExecutorIfPossible();
        }

        private synchronized void addNextTaskToExecutorIfPossible() {
            if (tasksExecuting < maxThreadsForResource) {
                Runnable task = tasksQueue.poll();
                if (task != null) {
                    tasksExecuting++;
                    executor.submit(() -> {
                        try {
                            task.run();
                        } finally {
                            handleTaskCompletion();
                        }
                    });
                }
            }
        }
    }
}
